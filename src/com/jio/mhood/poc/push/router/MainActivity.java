package com.jio.mhood.poc.push.router;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private Button routeBtn;
	private EditText etPackageName, etPushMesg;
	private String ACTION_PUSH_MESSAGE = "com.jio.mhood.push.intent.RECEIVE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		etPackageName = (EditText)findViewById(R.id.eTPackageUserInput);
		etPackageName.setText("com.jio.mhood.poc.push.receiver1");
		
		etPushMesg = (EditText)findViewById(R.id.eTPushUserInput);
		etPushMesg.setText("Hello World");

		routeBtn = (Button)findViewById(R.id.routeBtn);
		routeBtn.setOnClickListener(new OnClickListener() { 
			
			@Override
			public void onClick(View v) {  
				  checkPushReceiverPckg();
			}
		});
	}
	
	private boolean checkPushReceiverPckg() {
		
		PackageManager pm = getPackageManager();  
		String activityName = null;
		String activityPackageName;
        ArrayList<String> appPckgNames = new ArrayList<String>();
        ArrayList<String> mApplicationNames = new ArrayList<String>();
		Intent i = new Intent();
		i.setAction(ACTION_PUSH_MESSAGE);
		List<ResolveInfo> infos = pm.queryBroadcastReceivers(i, PackageManager.GET_RESOLVED_FILTER);
		 for (ResolveInfo info : infos) {
		        ActivityInfo activityInfo = info.activityInfo;
		        IntentFilter filter = info.filter;
		        if (filter != null && filter.hasAction(ACTION_PUSH_MESSAGE)) {
		            appPckgNames.add(activityInfo.packageName);
		            mApplicationNames.add(activityInfo.name);
		        }
		    }
		 boolean isSent = false;
		 for(int j=0; j<appPckgNames.size(); j++) {
			 
			 if(appPckgNames.get(j).contentEquals(etPackageName.getText().toString())){
				 Intent intent = new Intent();
				 intent.setAction(ACTION_PUSH_MESSAGE);
				 intent.setClassName(etPackageName.getText().toString(), mApplicationNames.get(j).toString());
				 intent.putExtra("pushMesg", etPushMesg.getText().toString());
				 sendBroadcast(intent);
				 isSent = true;
				 break;
			 }
		 }
		 if(isSent == false)
		 {
			 Toast.makeText(getApplicationContext(), "No Reciever Registered", Toast.LENGTH_SHORT).show();
		 }
        return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
